# cloud-storage

**Status: [Beta][beta]**

> - `google_cloud_support_feature_flag` (Beta) flag need to be enabled to use the Component.

The `cloud-storage` GitLab component uploads files to a pre-existing Google Cloud Storage bucket.

## Prerequisites

- Set up a Google Cloud workload identity pool and provider by following the steps in the GitLab tutorial [Google Cloud workload identity federation and IAM policies][onboarding].
- If you don't have a Google Cloud Storage bucket, then [create a Cloud Storage bucket][create-gcs-bucket].
- Configure your Cloud Storage bucket with [Uniform Bucket Level Access][uniform-bucket-level-access].
- If the Google Cloud Storage bucket is configured with [Requester Pays][requester-pays] then you must provide a `project-id` input and the caller must have the `roles/serviceusage.serviceUsageConsumer` role.

## Billing

Usage of the `cloud-storage` GitLab component might incur Google Cloud billing charges depending on your usage. For more information on Cloud Storage pricing, see
[Cloud Storage Pricing][pricing].

## Usage

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - component: gitlab.com/google-gitlab-components/cloud-storage/upload-cloud-storage@{VERSION}
    inputs:
      destination: myBucket/$CI_PIPELINE_ID
      path: testdata/
```

## Inputs

| Input | Description | Example| Default Value |
| ----- | ----------- | ------ | ------------- |
| `path` | (Required) The path to a file or folder to upload. If path is a file the single file is uploaded, if path is a folder all files in the path are uploaded | `myFile.txt` or `path/to/folder/` | |
| `destination` | (Required) Name of the bucket destination. Can be in the format of either `bucketName` or `bucketName/prefix`. If a prefix is provided, all objects uploaded will use the prefix in the object name | `bucketName` or `bucketName/prefix` | |
| `glob` | (Optional) Glob pattern to search for within the path parameter when path is a folder | `*.txt` | |
| `project-id` | (Optional) The Google Cloud project ID that will be used for quota or billing purposes. If set, the caller must have 'roles/serviceusage.serviceUsageConsumer' role | `my-project` | |
| `metadata-headers` | (Optional) Metadata headers to include with every Google Cloud Storage object upload. Headers must be provided as a list of key-value pairs. Settable fields are: `cache-control`, `content-disposition`, `content-encoding`, `content-language`, `content-type`, `custom-time`. A custom metadata field  prefixed with `x-goog-meta-` | In the format: `x-goog-meta-foo=bar,x-goog-meta-me=foobar` (no space) | |
| `concurrency` | (Optional) Number of files to simultaneously upload. | `50` | `100` |
| `gzip` | (Optional) Gzip files uploaded, defaults to true. This will override the `content-encoding` header to have the value of gzip, and will leave all other user provided headers as-is | `false` | `true` |
| `ignore-list` | (Optional) Processes the `.gcloudignore` file present at the top level of the repository. If true, the `.gcloudignore` file is parsed and any filepaths that match aren't uploaded to the storage bucket | `false` | `true` |
| `parent` | (Optional) Whether to include the base dir of the path parameter in the Google Cloud Storage object name | `false` | `true` |
| `stage` | (Optional) The GitLab CI/CD stage where the component will be executed | `my-stage` | `deploy` |
| `as` | (Optional) The name of the job to be executed | `my-job` | `upload-cloud-storage` |

## Authorization

To use the `cloud-storage` component, the workload identity pool must have the following minimum roles:
- Storage Object Creator ([`roles/storage.objectCreator`][role-storage-object-creator])

If you are providing a `project-id` to be used for quota or billing purposes, you also need the following role:
- Service Usage Consumer ([`roles/serviceusage.serviceUsageConsumer`][role-service-usage-consumer])

For example, to grant the `roles/storage.objectCreator` role to all principals in a workload identity pool matching `developer_access=true` attribute mapping by the gcloud CLI:

``` bash
# Replace ${PROJECT_ID}, ${PROJECT_NUMBER}, ${LOCATION}, ${POOL_ID} with your values below

WORKLOAD_IDENTITY=principalSet://iam.googleapis.com/projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/attribute.developer_access/true

gcloud projects add-iam-policy-binding ${PROJECT_ID} --member="${WORKLOAD_IDENTITY}" --role="roles/storage.objectCreator"
```

The bucket you are uploading to must be configured with [Uniform Bucket Level Access][uniform-bucket-level-access].

[beta]: https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta
[create-gcs-bucket]: https://cloud.google.com/storage/docs/creating-buckets
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html
[pricing]: https://cloud.google.com/storage/pricing
[requester-pays]: https://cloud.google.com/storage/docs/requester-pays
[role-storage-object-creator]: https://cloud.google.com/iam/docs/understanding-roles#storage.objectCreator
[role-service-usage-consumer]: https://cloud.google.com/iam/docs/understanding-roles#serviceusage.serviceUsageConsumer
[uniform-bucket-level-access]: https://cloud.google.com/storage/docs/using-uniform-bucket-level-access
